## Guides

At first look up our [Objective C Style Guide](https://bitbucket.org/zabeko/guides/src/57dbb89f5b14ba5c8f2d397c32da31a1f608d1ac/objective-c-style-guide.md?at=master)

At next, you should know our new [Branching Workflow](https://bitbucket.org/zabeko/guides/src/71525081c63a8e84c33ff91aaeffd78b7107ebcf/branching-guide.md?at=master)

So, now you are ready for [Contributing!](https://bitbucket.org/zabeko/guides/src/043fca242e78e21244accbdf4b213d6323790cc4/contributing-guide.md?at=master)