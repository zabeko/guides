# Branching guide

Common conept of branching described in the [nice article](https://habrahabr.ru/post/106912/)

## Structure

* origin
    * master
    * release
        * release-1-1-1
        * release-1-0-0
    * develop
    * chummy
        * develop
        * market
            * market
            * BW-2023-market-card
            * BW-2002-item-chart
        * bugs
              * BW-2062-build-161-bugs
    * my-chummy
        * develop
        * feature
            * feature
            * sub-feature

`develop` we call **root develop**  
`chummy/develop` and `my-chummy/develop` are **app develop** branches.  

For release preparing, we create release branch from `develop` with version number appended, for example `release/release-1-1-1`  
For feature development, we create feature branch from `app/develop` to the feature folder,
For example `develop/chummy/market/market`. It is **root feature branch**, you shold not commit to it directly,
only with pull requests.  
For commiting your changes use sub-branches from feature branch, created by Trello card,
for example `develop/chummy/market/BW-2002-item-chart`. We will cal it **feature sub-branch**  

Periodically we should sync develop branches. It should be organized event.

## Naming

### General
In general branch name is lowercased. Use dash instead of space. Other punctuation symbols are forbidden.

`root-folder/app-folder/feature-folder/feature-branch`

### Feature branch
If feature has trello card, branch name starts with uppercased Trello board prefix and card ID, then feature name lowercased. 

Construction:

`[Board prefix]-[Card ID]-[feature-name]`

For example if we have **Better World** board, then prefix will be **BW**, and card number is 4 digit number:

`BW-2023-market-card`

The same naming rule for bugfix branch

### Release branch
To release branch we append app version with 3 numbers, devided by dash

`release/release-1-1-1`

## Feature flow
1. Create folder for feature in the `develop/app` folder, create feature branch in this folder. At next, all feature sub-branches should be created in that folder. Create sub-branch for your task
1. Push all changes to the feature sub-branch, sync it with `app/develop`
1. Create PR from sub-branch to the feature branch
2. Resolve PR (details in Contributing guide), delete sub-branch
3. If all sub-branches merged, send build to the Fabric from feature-branch
4. If issues was found, please, create new sub-branch from your feature-branch for bugfixes
5. After applying bugfixes, it should be reviewd as well by PR
6. After all issues fixed, reviewed and tested, you can merge feature branch to the `app/develop` directly without PR

## Express develop-bugfix flow
If some bug found in `app/develop`, then bugfix flow almost the same as featrue flow, but without extra sub-branches

1. Create bugfix branch from `app/develop` in the `app/bugs` folder
1. Push all changes to the bugfix branch, sync it with `app/develop`
1. Create PR from bugfix branch to the `app/develop`
2. Resolve PR. Keep the branch.
3. Send build to the Fabric from `app/develop`
4. If issues was found, repeat this flow with the same bugfix branch
6. After all issues fixed, reviewed, merged, delete bugfix branch

## Release flow
When `app/develop` is stable and has all features for next release, we create release branch:

0. Sync `app/develop` with `develop`
1. Create `release/release-x-x-x` branch from `develop` with version number
2. Send build to the Fabric
4. If issues was found, please, create new sub-branch from  `release/release-x-x-x` for bugfixes.
5. Push changes to the new branch, create PR to the `release/release-x-x-x`
6. Resolve PR, delete sub-branch
8. Send build to the Fabric
9. When released, merge `release/release-x-x-x` to the `master` with tagged version. Also merge it to the all develop branches. Delete `release/release-x-x-x`

## Express release-bugfix flow
If some bug found in current release (it is `master`) and we need quickly send new release version, then flow is next:

1. Create `release/release-x-x-x` branch from `master`
2. Create `release/BOARDPREFIX-CARDID-bugfix-name` sub-branch from `release/release-x-x-x`
3. Push changes to the sub-branch, create PR to the `release/release-x-x-x`
6. Resolve PR, delete sub-branch
8. Send build to the Fabric
9. When released, merge `release/release-x-x-x` to the `master` with tagged version. Also merge it to the all develop branches. Delete `release/release-x-x-x`

