#Contributing Guide

##Introduction
PR it is Pull Request. You shold create it after you finish work with sub-branch of feature-branch (See details in [branching guide](https://bitbucket.org/zabeko/guides/src/da438621761b3525a4b9216180bade8b44eb28ce/branching-guide.md?at=master))

I assume that you are familiar with PR creation. 
It is desribed [here](https://confluence.atlassian.com/bitbucket/work-with-pull-requests-223220593.html)

##How to post PR

###Fields
####Title

`Board Prefix` + `Card ID` + `Task Title`

For example:

`BW-2023 Карточка магазина`

It the same as branch name, but uppercased letters and punctuation allowed

####Description
Should contain next items:

2. Brief task description
3. Breef implementation description
4. Which modules are affected

####Reviwers
Add all developers from your team exept yourself

####Close branch
Check it

###After post
Notify your team channel about new PR

###During review
Notify your commentators about fixes.
You should receive 2 approvals to able to merge

###After successful review
During merge, choose **Squash** merge strategy and prettify commit message.


##How to review PR
### Please check PR for next statements
* PR description matches scheme above
* PR description has enough details to understand what is done
* Task info matches trello's task
* Code matches [Style Guide](https://bitbucket.org/zabeko/guides/src/57dbb89f5b14ba5c8f2d397c32da31a1f608d1ac/objective-c-style-guide.md?at=master)
* Whole code belongs to the current task. No other "bugfixes" or "parallels tasks". One PR - one task

Optionally you can switch to the merging branch, build and run the code, check for compile errors/warnings, check task implementation in runtime. It is very good practice.

If everything is ok, approve request, else write comments.
Notify PR creator about your review.